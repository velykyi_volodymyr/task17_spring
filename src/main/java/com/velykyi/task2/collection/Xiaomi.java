package com.velykyi.task2.collection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(2)
@Qualifier("xiaomi")
public class Xiaomi implements Phone {
    @Override
    public String getPhone() {
        return "Xiaomi";
    }
}
