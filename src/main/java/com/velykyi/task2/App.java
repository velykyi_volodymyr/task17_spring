package com.velykyi.task2;


import com.velykyi.task2.beans1.*;
import com.velykyi.task2.beans2.RoseFlower;
import com.velykyi.task2.collection.PhoneCollection;
import com.velykyi.task2.pojo.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {
    private static Logger logger1 = LogManager.getLogger(com.velykyi.App.class);
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.getEnvironment().setActiveProfiles("test");
        context.register(Config4.class);
        context.getEnvironment().setActiveProfiles("dev");
        context.register(Config1.class);
        context.register(Config3.class);
        context.getEnvironment().setActiveProfiles("prod");
        context.register(Config2.class);
        context.refresh();
        logger1.info(context.getBean(RoseFlower.class));
        logger1.info(context.getBean(BeanA.class));
        logger1.info(context.getBean(BeanB.class));
        logger1.info(context.getBean(BeanC.class));
        logger1.info(context.getBean(Bean1.class));
        try{
            logger1.info(context.getBean(Bean2.class));
        } catch (Exception e) {
            logger1.info("Bean2 is not exist.");
        }
        context.getBean(PhoneCollection.class).printList();
    }
}
