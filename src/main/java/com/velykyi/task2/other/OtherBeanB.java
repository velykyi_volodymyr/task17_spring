package com.velykyi.task2.other;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Qualifier("OtherB")
@Scope("prototype")
public class OtherBeanB {
}
