package com.velykyi.pojo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.*;

public class BeanE extends Bean {
    private static Logger logger1 = LogManager.getLogger(BeanA.class);

    public BeanE(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @PostConstruct
    public void postConstruct() {
        logger1.info(this + " on postConstruct().");
    }

    @PreDestroy
    public void preDestroy() {
        logger1.info(this + " on preDestroy().");
    }

    @Override
    public String toString() {
        return "BeanE{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
