package com.velykyi.task2.collection;

import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
@Primary
public class Samsung implements Phone {
    @Override
    public String getPhone() {
        return "Samsung";
    }
}
