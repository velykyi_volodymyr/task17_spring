package com.velykyi.task2.collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PhoneCollection {
    private static Logger logger1 = LogManager.getLogger(com.velykyi.task2.App.class);
    @Autowired
    private List<Phone> phones;

    public void printList() {
        for (Phone phone : phones) {
            logger1.info(phone.getPhone());
        }
    }
}
