package com.velykyi.task2.beans1;

import com.velykyi.task2.other.OtherBeanA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BeanA {
    private OtherBeanA otherBeanA1;
    private OtherBeanA otherBeanA2;

    @Autowired
    public BeanA(OtherBeanA otherBeanA1, OtherBeanA otherBeanA2) {
        this.otherBeanA1 = otherBeanA1;
        this.otherBeanA2 = otherBeanA2;
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "otherBeanA1=" + otherBeanA1 +
                ", otherBeanA2=" + otherBeanA2 +
                '}';
    }
}
