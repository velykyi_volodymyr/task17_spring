package com.velykyi;

import com.velykyi.pojo.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class CustomerBeanPostProcessor implements BeanPostProcessor {
    private static Logger logger1 = LogManager.getLogger(App.class);
    @Override
    public Object postProcessBeforeInitialization(Object o, String s) throws BeansException {
        logger1.info(">>PostProcessorBeforeInitialization()");
        try {
            Bean bean = (Bean) o;
            if (bean.getName() == null) {
                logger1.info(bean + "'s name is null.");
                bean.setName("NOT NULL");
            }
            if (bean.getValue() < 0) {
                logger1.info(bean + "'s value is < 0.");
                bean.setValue(0);
            }
            logger1.info(bean);
            return bean;
        } catch (ClassCastException e){
        logger1.info("Bean " + o + ", beanName " + s);
        return o;
    }
    }

    @Override
    public Object postProcessAfterInitialization(Object o, String s) throws BeansException {
        logger1.info(">>PostProcessorAfterInitialization()");
        try{
            Bean bean = (Bean) o;
            logger1.info(bean);
            return bean;
        } catch (ClassCastException e){
            logger1.info("Bean " + o + ", beanName " + s);
            return o;
        }
    }
}
