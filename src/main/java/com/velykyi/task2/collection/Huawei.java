package com.velykyi.task2.collection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(Ordered.LOWEST_PRECEDENCE)
@Qualifier("huawei")
public class Huawei implements Phone {
    @Override
    public String getPhone() {
        return "Huawei";
    }
}
