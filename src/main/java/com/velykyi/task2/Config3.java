package com.velykyi.task2;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan("com.velykyi.task2.collection")
public class Config3 {
}
