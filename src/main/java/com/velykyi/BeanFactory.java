package com.velykyi;

import com.velykyi.pojo.BeanB;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.GenericBeanDefinition;

public class BeanFactory implements BeanFactoryPostProcessor {
    private static Logger logger1 = LogManager.getLogger(App.class);

    public void init() {
        logger1.info("BeanFactory init()");
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {
//        GenericBeanDefinition genericBeanDefinition = (GenericBeanDefinition) configurableListableBeanFactory.getBean("BeanB");
//                .getBeanDefinition("getBeanB");
//        genericBeanDefinition.setInitMethodName("newInitMethod");
        for (String beanName : configurableListableBeanFactory.getBeanDefinitionNames()) {
            BeanDefinition beanDefinition = configurableListableBeanFactory
                    .getBeanDefinition(beanName);
            logger1.info(beanDefinition);
        }
    }
}
