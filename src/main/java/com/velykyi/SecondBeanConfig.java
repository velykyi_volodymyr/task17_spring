package com.velykyi;


import com.velykyi.pojo.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.annotation.*;
import org.springframework.context.annotation.Bean;

@Import(BeanConfig.class)
public class SecondBeanConfig {

    @Qualifier("BeanA1")
    @Bean
    @DependsOn(value = {"getBeanD", "getBeanB", "getBeanC"})
    public BeanA getBeanA1(BeanB beanB, BeanC beanC){
        String nameA = beanB.getName();
        int valueA = beanC.getValue();
        return new BeanA(nameA, valueA);
    }

    @Qualifier("BeanA2")
    @Bean
    public BeanA getBeanA2(BeanB beanB, BeanD beanD){
        String nameA = beanD.getName();
        int valueA = beanB.getValue();
        return new BeanA(nameA, valueA);
    }

    @Qualifier("BeanA3")
    @Bean
    public BeanA getBeanA3(BeanC beanC, BeanD beanD){
        String nameA = beanC.getName();
        int valueA = beanD.getValue();
        return new BeanA(nameA, valueA);
    }

    @Bean
    public BeanE getBeanE1(BeanA getBeanA1){
        String nameE = getBeanA1.getName();
        int valueE = getBeanA1.getValue();
        return new BeanE(nameE, valueE);
    }

    @Bean
    public BeanE getBeanE2(BeanA getBeanA2){
        String nameE = getBeanA2.getName();
        int valueE = getBeanA2.getValue();
        return new BeanE(nameE, valueE);
    }

    @Bean
    public BeanE getBeanE3(BeanA getBeanA3){
        String nameE = getBeanA3.getName();
        int valueE = getBeanA3.getValue();
        return new BeanE(nameE, valueE);
    }

    @Bean
    @Lazy
    public BeanF getBeanF1(@Qualifier("BeanA1") BeanA beanA){
        int valueF = beanA.getValue();
        return new BeanF(null, valueF);
    }

    @Bean
    @Lazy
    public BeanF getBeanF2(@Qualifier("BeanA2")BeanA beanA){
        String nameF = beanA.getName();
        int valueF = beanA.getValue();
        return new BeanF(nameF, valueF);
    }

    @Bean
    @Lazy
    public BeanF getBeanF3(@Qualifier("BeanA3")BeanA beanA){
        String nameF = beanA.getName();
        int valueF = beanA.getValue();
        return new BeanF(nameF, valueF);
    }

    @Bean(initMethod = "init")
    public BeanFactory getBeanFactory(){
        return new BeanFactory();
    }

    @Bean
    public CustomerBeanPostProcessor getCustomerBeanPostProcessor() {
        return new CustomerBeanPostProcessor();
    }
}
