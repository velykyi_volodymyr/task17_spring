package com.velykyi.task2;

import com.velykyi.task2.pojo.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class Config4 {

    @Bean
    @Profile("Bean1")
    public Bean1 bean1() {
        return new Bean1();
    }

    @Bean
    @Profile("test")
    public Bean2 bean2() {
        return new Bean2();
    }
}
