package com.velykyi.task2.collection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(4)
@Qualifier("honor")
public class Honor implements Phone {
    @Override
    public String getPhone() {
        return "Honor";
    }
}
