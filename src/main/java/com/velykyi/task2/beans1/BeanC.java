package com.velykyi.task2.beans1;

import com.velykyi.task2.other.OtherBeanC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BeanC {
    @Autowired
    private OtherBeanC otherBeanC1;
    @Autowired
    private OtherBeanC otherBeanC2;

    @Override
    public String toString() {
        return "BeanC{" +
                "otherBeanC1=" + otherBeanC1 +
                ", otherBeanC2=" + otherBeanC2 +
                '}';
    }
}
