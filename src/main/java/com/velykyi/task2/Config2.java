package com.velykyi.task2;

import com.velykyi.task2.beans3.*;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Profile;


@Configuration
@ComponentScan(includeFilters = {@Filter(type = FilterType.REGEX, pattern = ".*Flower"),
        @Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {BeanD.class, BeanF.class})})
@Profile("prod")
public class Config2 {
}
