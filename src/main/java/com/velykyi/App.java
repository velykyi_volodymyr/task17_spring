package com.velykyi;

import com.velykyi.pojo.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {
    private static Logger logger1 = LogManager.getLogger(App.class);
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(SecondBeanConfig.class);
        BeanF beanF = (BeanF) context.getBean("getBeanF1");
        logger1.info(beanF);
    }
}
