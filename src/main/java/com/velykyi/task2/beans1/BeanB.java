package com.velykyi.task2.beans1;

import com.velykyi.task2.other.OtherBeanB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class BeanB {
    private OtherBeanB otherBeanB1;
    private OtherBeanB otherBeanB2;

    @Autowired
    @Qualifier("OtherB")
    public void setOtherBeanB1(OtherBeanB otherBeanB1) {
        this.otherBeanB1 = otherBeanB1;
    }

    @Autowired
    @Qualifier("OtherB")
    public void setOtherBeanB2(OtherBeanB otherBeanB2) {
        this.otherBeanB2 = otherBeanB2;
    }

    @Override
    public String toString() {
        return "BeanB{" +
                "otherBeanB1=" + otherBeanB1 +
                ", otherBeanB2=" + otherBeanB2 +
                '}';
    }
}
