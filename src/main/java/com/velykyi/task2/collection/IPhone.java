package com.velykyi.task2.collection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(3)
@Qualifier("iphone")
public class IPhone implements Phone {
    @Override
    public String getPhone() {
        return "iPhone";
    }
}
