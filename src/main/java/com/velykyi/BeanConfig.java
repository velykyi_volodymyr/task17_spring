package com.velykyi;

import com.velykyi.pojo.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.annotation.*;
import org.springframework.context.annotation.Bean;

@Configuration
@PropertySource("beans.properties")
public class BeanConfig {
    @Value("${beanB.name}")
    private String nameB;
    @Value("${beanB.value}")
    private int valueB;
    @Value("${beanC.name}")
    private String nameC;
    @Value("${beanC.value}")
    private int valueC;
    @Value("${beanD.name}")
    private String nameD;
    @Value("${beanD.value}")
    private int valueD;

    @Bean(initMethod = "initMethod", destroyMethod = "destroyMethod")
    public BeanB getBeanB(){
        return new BeanB(nameB, valueB);
    }

    @Bean(initMethod = "initMethod", destroyMethod = "destroyMethod")
    public BeanC getBeanC(){
        return new BeanC(nameC, valueC);
    }

    @Bean(initMethod = "initMethod", destroyMethod = "destroyMethod")
    public BeanD getBeanD(){
        return new BeanD(nameD, valueD);
    }

}
