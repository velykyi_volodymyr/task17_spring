package com.velykyi.pojo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.*;

public class BeanA extends Bean implements InitializingBean, DisposableBean {
    private static Logger logger1 = LogManager.getLogger(BeanA.class);

    public BeanA(String name, int value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public void destroy() throws Exception {
        logger1.info(this + " on destroy().");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        logger1.info(this + " on afterPropertiesSet().");
    }
}
