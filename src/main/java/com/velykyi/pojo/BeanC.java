package com.velykyi.pojo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BeanC extends Bean {
    private static Logger logger1 = LogManager.getLogger(BeanA.class);

    public BeanC(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public void initMethod(){
        logger1.info(this + " on initMethod()");
    }

    public void destroyMethod(){
        logger1.info(this + " on destroyMethod()");
    }

    @Override
    public String toString() {
        return "BeanC{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
