package com.velykyi.task2;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ComponentScan("com.velykyi.task2.beans1")
@ComponentScan("com.velykyi.task2.other")
@Profile("dev")
public class Config1 {
}
